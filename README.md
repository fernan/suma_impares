num1 = int(input("dame un numero entero no negativo: "))
num2 = int(input("Dame otro número entero: "))

sumaimpares = 0

for num in range(num1, num2+1):
    if num % 2 != 0:  # Verificar si el número es impar
        sumaimpares += num


print("La suma de los números impares comprendidos en este intervalo es:", sumaimpares)